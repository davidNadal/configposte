import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-display-board',
  templateUrl: './display-board.component.html',
  styleUrls: ['./display-board.component.css']
})
export class DisplayBoardComponent implements OnInit {

  constructor() { }

  @Input() recetteCount = 0;
  @Output() getRecettesEvent = new EventEmitter();

  ngOnInit(): void {
  }

  getAllRecettes() {
    this.getRecettesEvent.emit('récupérer les recettes');
  }

}
