import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from './app.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  constructor(private appService: AppService) {}

  title = 'angular-nodejs-example';

  recetteForm = new FormGroup({
    titre: new FormControl('', Validators.nullValidator && Validators.required),
    ingredient1: new FormControl('', Validators.nullValidator && Validators.required),
    ingredient2: new FormControl('', Validators.nullValidator && Validators.required),
    ingredient3: new FormControl('', Validators.nullValidator && Validators.required),
  });

  recettes: any[] = [];
  recetteCount = 0;

  destroy$: Subject<boolean> = new Subject<boolean>();

  onSubmit() {
    this.appService.addRecette(this.recetteForm.value, this.recetteCount + 1).pipe(takeUntil(this.destroy$)).subscribe(data => {
      console.log('message::::', data);
      this.recetteCount = this.recetteCount + 1;
      console.log(this.recetteCount);
      this.recetteForm.reset();
    });
  }

  getAllRecettes() {
    this.appService.getRecettes().pipe(takeUntil(this.destroy$)).subscribe((recettes: any[]) => {
		this.recetteCount = recettes.length;
        this.recettes = recettes;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit() {
	this.getAllRecettes();
  }
}
