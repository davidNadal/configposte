DROP TABLE IF EXISTS recette;

CREATE TABLE recette (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  titre varchar(255) NULL,
  ingredient1 varchar(255) NULL,
  ingredient2 varchar(255) NULL,
  ingredient3 varchar(255) NULL,
  ingredient4 varchar(255) NULL,
  ingredient5 varchar(255) NULL,
  ingredient6 varchar(255) NULL,
  ingredient7 varchar(255) NULL,
  ingredient8 varchar(255) NULL,
  ingredient9 varchar(255) NULL,
  ingredient10 varchar(255) NULL
);

INSERT INTO recette (titre, ingredient1, ingredient2) VALUES('nuggets', 'poulet', 'huile');