package com.configposte.recettes.links;

import org.springframework.stereotype.Component;

@Component
public class RecetteLinks {
	
    public static final String LIST_RECETTES = "/recettes";
    public static final String ADD_RECETTE = "/recette";

}
