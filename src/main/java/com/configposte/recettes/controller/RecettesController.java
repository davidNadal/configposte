package com.configposte.recettes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.configposte.recettes.entity.Recettes;
import com.configposte.recettes.links.RecetteLinks;
import com.configposte.recettes.service.RecettesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/")
public class RecettesController {

    @Autowired
    RecettesService recettesService;

    @GetMapping(path = RecetteLinks.LIST_RECETTES)
    public ResponseEntity<?> listRecettes() {
        log.info("RecettesController:  list recettes");
        List<Recettes> resource = recettesService.getRecettes();
        return ResponseEntity.ok(resource);
    }

    @PostMapping(path = RecetteLinks.ADD_RECETTE)
    public ResponseEntity<?> saveRecette(@RequestBody Recettes recette) {
        Recettes resource = recettesService.saveRecette(recette);
        return ResponseEntity.ok(resource);
    }
}
