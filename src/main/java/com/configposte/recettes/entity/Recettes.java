package com.configposte.recettes.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;


import lombok.Data;

@Data
public class Recettes implements Serializable {
	
    @Id
    @Column
    private int id;

    @Column
    private String titre;
    
    @Column
    private String ingredient1;
    
    @Column
    private String ingredient2;
        
    @Column
    private String ingredient3;
        
    @Column
    private String ingredient4;
        
    @Column
    private String ingredient5;
        
    @Column
    private String ingredient6;
        
    @Column
    private String ingredient7;
        
    @Column
    private String ingredient8;    
    
    @Column
    private String ingredient9;    
    
    @Column
    private String ingredient10;
}
