package com.configposte.recettes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.configposte.recettes.entity.Recettes;

@RepositoryRestResource()
public interface RecettesRepository extends JpaRepository<Recettes, Integer>, JpaSpecificationExecutor<Recettes>, QuerydslPredicateExecutor<Recettes> {}
