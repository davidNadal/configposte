package com.configposte.recettes.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.configposte.recettes.entity.Recettes;
import com.configposte.recettes.repository.RecettesRepository;

@Component
public class RecettesService {

    private final RecettesRepository recettesRepository;

    public RecettesService(RecettesRepository recettesRepository) {
        this.recettesRepository = recettesRepository;
    }

    public List<Recettes> getRecettes() {
        return recettesRepository.findAll();
    }

    public Recettes saveRecette(Recettes recettes) {
        return recettesRepository.save(recettes);
    }

}
